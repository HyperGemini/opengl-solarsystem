## Prerequisites

1. GLEW library installed
```
sudo apt-get install libglew-dev
```
2. GLFW library installed
```
sudo apt-get install libglfw3
sudo apt-get install libglfw3-dev
```
3. glm library installed (single header library)
```
sudo apt-get install libglm-dev
```
4. OpenAL C++ implementation library installed
```
sudo apt-get install libopenal-dev
```
5. ALUT library installed
```
sudo apt-get install libalut-dev
```
